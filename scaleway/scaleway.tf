provider "scaleway" {
  organization = "${var.scaleway_organization}"
  token = "${var.scaleway_token}"
  region = "ams1"
}

data "scaleway_image" "centOS-76" {
  architecture = "x86_64"
  name = "CentOS 7.6"
}

resource "scaleway_ip" "ip" {
}

resource "scaleway_ssh_key" "default" {
  key = "${file("def_key.pub")}"
}

resource "scaleway_server" "my_server" {
    name = "my_server"
    public_ip = "${scaleway_ip.ip.ip}"
    image = "${data.scaleway_image.centOS-76.id}"
    type = "DEV1-S"
}
