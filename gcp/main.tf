provider "google" {
  credentials = "${file("credentials.json")}"
  project = "substantial-art-245719"
  region = "europe-west3"
}

resource "google_compute_instance" "default" {
  name = "basic-micro"
  machine_type = "f1-micro"
  zone = "europe-west3-a"

  metadata = {
    ssh-keys = "${var.username}:${file("def_key.pub")}"
  }

  boot_disk {
    initialize_params {
      image = "centos-7"
    }
  }

  network_interface {
    network = "default"

    access_config {
      // Include this section to give the VM an external ip address
    }
  }
}

output "gcp-ip-basic-micro-1" {
 value = "${google_compute_instance.default.network_interface.0.access_config.0.nat_ip}"
}

resource "google_compute_instance" "free-tier" {
  name = "basic-micro-us"
  machine_type = "f1-micro"
  zone = "us-east1-c"

  metadata = {
    ssh-keys = "${var.username}:${file("def_key.pub")}"
  }

  boot_disk {
    initialize_params {
      image = "centos-7"
    }
  }

  network_interface {
    network = "default"

    access_config {
      // Include this section to give the VM an external ip address
    }
  }
}

output "gcp-ip-basic-micro-backup" {
 value = "${google_compute_instance.free-tier.network_interface.0.access_config.0.nat_ip}"
}
