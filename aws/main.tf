resource "aws_instance" "base_ec2_1" {
  ami             = "${data.aws_ami.centos.id}"
  instance_type   = "t2.micro"
  key_name = "${aws_key_pair.def_key.key_name}"

  security_groups = [
    "${aws_security_group.allow_ssh.name}",
    "${aws_security_group.allow_outbound.name}",
    "${aws_security_group.allow_zerotier.name}",
    "${aws_security_group.allow_http.name}"
  ]

  root_block_device {
  delete_on_termination = true
  }
}
