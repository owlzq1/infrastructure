resource "aws_key_pair" "def_key" {
  key_name = "def_key"
  public_key = "${file("def_key.pub")}"
}
